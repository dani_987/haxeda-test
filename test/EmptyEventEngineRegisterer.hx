package test;

import test.stories.CanSendMultipleEventsAtOnce.ReceiveSomeValueChangeListener;
import test.stories.CanWaitForAnEvent.CanWaitForAnEventListener;
import haxeda.EventListener;
import haxeda.EventEngine;
import haxeda.EventEngineRegisterer;
import haxeda.events.order.EventId;

import test.events.*;

class TestPasses implements EventListener {
	private var eventEngine: EventEngine;

	public function new(eventEngine: EventEngine) {
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	@listener("TestStart")
	public function onEvent(_:{}, sourceEventId:EventId):EventProcessingResult {
		eventEngine.triggerEvent(new TestDone(), sourceEventId);
		return Success;
	}
}
class EmptyEventEngineRegisterer implements EventEngineRegisterer {
    public function new(){}

	public function registerToEventEngine(eventEngineToRegister:EventEngine):Bool {
		#if test
			new TestPasses(eventEngineToRegister);
			new CanWaitForAnEventListener(eventEngineToRegister);
			ReceiveSomeValueChangeListener.instance.setupEventEngine(eventEngineToRegister);
		#end
		return true;
	}
}
