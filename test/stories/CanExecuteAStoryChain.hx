package test.stories;

import haxeda.events.UserEventBuilder;
import haxeda.EventEngine;
import haxeda.EventListener;
import events.TestSomeOtherEvent;
import haxeda.events.ApplicationStarted;
import test.stories.CanSendMultipleEventsAtOnce.ReceiveSomeValueChangeListener;
using haxeda.test.UserStory;
import haxeda.events.order.EventId;

class CanExecuteAStoryChain extends haxeda.test.UserStoryCreator {

    public function new() {}

    function createStory():UserStory {
        var steps = 0;
        return this.givenAppStart().whenAll(
            e -> [
                new TestSomeOtherEvent(5),
                new test.events.TestStart()
            ]
        ).andWhenUserEvent(
            "TestDone", (e) -> {
                if(!ReceiveSomeValueChangeListener.instance.checkSomeValue(5))return null;
                if(steps != 0)return null;
                steps++;
                return [
                    new TestSomeOtherEvent(3),
                    new test.events.TestStart()
                ];
            }
        ).andWhenUserEvent(
            "TestDone", (e) -> {
                if(!ReceiveSomeValueChangeListener.instance.checkSomeValue(3))return null;
                if(steps != 1)return null;
                steps++;
                return [
                    new SomeOtherTestEnd()
                ];
            }
        ).thenUserEvent(
            "SomeOtherTestEnd", (e) -> {
                return ReceiveSomeValueChangeListener.instance.checkSomeValue(3)
                    && steps == 2;
            }
        ).isSuccessful();
    }
}

class SomeOtherTestStart implements UserEventBuilder {}
class SomeOtherTestEnd implements UserEventBuilder {}

class ChainEventListener implements EventListener {
    public static var instance = new ChainEventListener();
    private function new() {}
    private var eventEngine: EventEngine;

	public function setupEventEngine(eventEngine: EventEngine) {
        this.eventEngine = eventEngine;
		this.registerToEventEngine(this.eventEngine);
	}

    @listener("SomeOtherTestStart")
    private function onSomeOtherTestStarted(_:{}, sourceEventId:EventId) : EventProcessingResult {
        this.eventEngine.triggerEvent(new SomeOtherTestEnd(), sourceEventId);
        return Success;
    }
}
