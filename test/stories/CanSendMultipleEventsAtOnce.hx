package test.stories;

import haxeda.EventEngine;
import haxeda.EventListener;
import events.TestSomeOtherEvent;
import haxeda.events.ApplicationStarted;
import haxeda.test.UserStoryCreator;
using haxeda.test.UserStory;

class CanSendMultipleEventsAtOnce extends UserStoryCreator {
    public function new() {}

    public function createStory() : UserStory {
        return this.givenAppStart().whenAll(
            e -> [
                new TestSomeOtherEvent(5),
                new test.events.TestStart(),
            ]
        ).thenUserEvent(
            "TestDone", e -> ReceiveSomeValueChangeListener.instance.checkSomeValue(5)
        ).isSuccessful();
    }
}

class ReceiveSomeValueChangeListener implements EventListener {
    @update private var someValue : Int = 0;
    public static var instance = new ReceiveSomeValueChangeListener();
    private function new() {}

	public function setupEventEngine(eventEngine: EventEngine) {
		this.registerToEventEngine(eventEngine);
	}

    public function checkSomeValue(toCheckWith: Int) : Bool {
        return this.someValue == toCheckWith;
    }
}
