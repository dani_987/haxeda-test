package test.stories;
import haxeda.events.ApplicationStarted;
using haxeda.test.UserStory;

class CanCreateAUserStory extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.givenAppStart().when(
                e -> new test.events.TestStart()
            ).thenUserEvent(
                "TestDone", (e) -> true
            ).isSuccessful();
    }
}
