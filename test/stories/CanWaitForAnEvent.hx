package test.stories;

import haxeda.events.order.EventId;
import haxeda.EventEngine;
import sys.thread.Thread;
import events.TestProcessOtherThread;
import haxeda.events.ApplicationStarted;
import haxeda.EventListener;
using haxeda.test.UserStory;

class CanWaitForAnEvent extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.givenAppStart().when(
                e ->  new TestProcessOtherThread()
            ).thenUserEvent(
               "TestDone", e -> true
            ).isSuccessful().andWaitForEndEvent(2);
    }
}

class CanWaitForAnEventListener implements EventListener {
	private var eventEngine: EventEngine;

	public function new(eventEngine: EventEngine) {
		this.eventEngine = eventEngine;
		this.registerToEventEngine(eventEngine);
	}

	@listener("TestProcessOtherThread")
	public function onEvent(_:{}, sourceEventId:EventId):EventProcessingResult {
		Thread.create(() -> {
            Sys.sleep(1);
            eventEngine.triggerEvent(new test.events.TestDone(), sourceEventId);
        });
		return Success;
	}
}
