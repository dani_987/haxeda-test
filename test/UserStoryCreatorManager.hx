package test;

class UserStoryCreatorManager implements haxeda.test.UserStoryCreatorCollectionManager{
    public function new() {}

    public function getAllCreators() : Array<haxeda.test.UserStoryCreator> {
        return [
            new test.stories.CanCreateAUserStory(),
            new test.stories.CanWaitForAnEvent(),
            new test.stories.CanSendMultipleEventsAtOnce(),
            new test.stories.CanExecuteAStoryChain(),
        ];
    }
}
