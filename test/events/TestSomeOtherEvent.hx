package events;

import haxeda.events.ServerEvent;

class TestSomeOtherEvent implements ServerEvent {
    @update private var someValue : Int;
    public function new(someValue: Int) {
        this.someValue = someValue;
    }
}
