package haxeda.test;

import haxeda.events.Event;
import haxeda.Classes;

@:generic
@:allow(haxeda.test.UserStoryCreator.given)
class UserStoryGiven<Given:Event> {
    private var givenClass: Class<Event>;
    private var storyName: String;

    public function new(storyCreator: haxeda.test.UserStoryCreator, givenClass: Class<Given>) {
        this.storyName = Classes.getSimpleName(Classes.fromInstance(storyCreator));
        this.givenClass = cast givenClass;
    }

    @:generic
    public function when<When:Event>( whenFun: Given->When) : haxeda.test.UserStoryWhen {
        return new haxeda.test.UserStoryWhen(
            storyName,
            Classes.getFullName(givenClass),
            DataEvent((e)->{return [whenFun(cast e)];}),
        );
    }

    public function whenAll( whenFunList: Given->Array<Event>) : haxeda.test.UserStoryWhen {
        return new haxeda.test.UserStoryWhen(
            storyName,
            Classes.getFullName(givenClass),
            DataEvent(cast whenFunList),
        );
    }
}
@:allow(haxeda.test.UserStoryCreator.givenUserEvent)
class UserStoryGivenUserEvent {
    private var givenName: String;
    private var storyName: String;

    public function new(storyCreator: haxeda.test.UserStoryCreator, givenName: String) {
        this.storyName = Classes.getSimpleName(Classes.fromInstance(storyCreator));
        this.givenName = givenName;
    }

    public function when( whenFun: {}->Event) : haxeda.test.UserStoryWhen {
        return new haxeda.test.UserStoryWhen(
            storyName,
            givenName,
            UserEvent((e)->{return [whenFun(e)];}),
        );
    }

    public function whenAll( whenFunList: {}->Array<Event>) : haxeda.test.UserStoryWhen {
        return new haxeda.test.UserStoryWhen(
            storyName,
            givenName,
            UserEvent(whenFunList),
        );
    }
}
