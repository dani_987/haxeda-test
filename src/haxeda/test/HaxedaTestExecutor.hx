package haxeda.test;
import haxeda.EventListener;
import haxeda.events.ApplicationStarted;
import haxeda.events.Event;
import haxeda.test.UserStory;
import haxeda.events.order.EventId;
using buddy.Should;

class HaxedaTestListener implements haxeda.EventListener {
    private var executor : HaxedaTestExecutor;
    private var baseStory : UserStory;
    private var stories : List<UserStory>;
    private var nextSteps : List<UserStory>;
    private var currentStep : UserStory;
    private var eventEngine: EventEngine;
    private var listeningOn : List<String>;
    private var substepPosition : Int;

    public function new(
        executor: HaxedaTestExecutor,
        baseStory: UserStory,
        eventEngine: EventEngine,
        executeStories: List<UserStory>
    ) {
        this.executor = executor;
        this.baseStory = baseStory;
        this.eventEngine = eventEngine;
        this.stories = executeStories;
        //copy list
        this.nextSteps = executeStories.map((e) -> {return e;});
        this.currentStep = null;
        this.substepPosition = -1;

        this.listeningOn = new List<String>();

        eventEngine.registerGenericFunction( "ApplicationStarted", this, this.onEvent);
    }

    private function addEventToListenOn(toListenOn: String){
        if(listeningOn.filter(e -> (e == toListenOn)).isEmpty()){
            listeningOn.add(toListenOn);
            eventEngine.registerGenericFunction( toListenOn, this, this.onEvent);
        }
    }

    private function removeEventToListenOn(toListenOn: String){
        if(!listeningOn.filter(e -> (e == toListenOn)).isEmpty()){
            listeningOn.remove(toListenOn);
            eventEngine.removeGenericFunction(toListenOn, this);
        }
    }

    public function onEvent(eventOccured: Event, sourceEventId: EventId) : EventProcessingResult {
        var story = this.nextSteps.first();
        if(currentStep != null){
            if(currentStep.getSubstepCount() > substepPosition && substepPosition >= 0)
            {
                var expectedEvent = currentStep.getExpectedSubstepEventName(substepPosition);
                if(expectedEvent != eventOccured.__getEventName()){
                    trace('Got unexpected Substep-Event ${Classes.getFullName(Classes.fromInstance(eventOccured))} for next Step ${currentStep.getName()} Substep ${this.substepPosition}');
                    return WithError('Unexpected Substep-Event ${Classes.getFullName(Classes.fromInstance(eventOccured))}');
                }
                removeEventToListenOn(expectedEvent);
                var nextEvents = currentStep.getSubstepEvents(substepPosition, eventOccured);
                if(nextEvents == null){
                    this.onSubstepErrorAccured(currentStep, eventOccured, substepPosition);
                    substepPosition++;
                } else {
                    this.onSubstepWasProcessed(currentStep, eventOccured, substepPosition);
                    substepPosition++;
                    addEventListener();
                    for(nextEvent in nextEvents){
                        eventEngine.triggerEvent(nextEvent, sourceEventId);
                    }
                }
                return Success;
            }
            this.onUserStoryGotExpectedEvent(currentStep, eventOccured);
        }
        currentStep = story;
        if(story != null){
            if(story.getGivenEventName() == eventOccured.__getEventName()){
                this.nextSteps.remove(story);
                substepPosition = 0;
                removeEventToListenOn(story.getGivenEventName());
                addEventListener();
                this.triggerStory(eventOccured, story, sourceEventId);
                return Success;
            } else {
                trace('Got unexpected Event ${Classes.getFullName(Classes.fromInstance(eventOccured))} for next Step ${story.getName()}');
                return WithError('Unexpected Event ${Classes.getFullName(Classes.fromInstance(eventOccured))}');
            }
        } else {
            return Success;
        }
    }

    private function addEventListener() {
        if(currentStep.getSubstepCount() > substepPosition){
            addEventToListenOn(currentStep.getExpectedSubstepEventName(substepPosition));
        } else {
            addEventToListenOn(currentStep.getExpectedEventName());
        }
    }

    private function triggerStory(triggeredBy: Event, storyToTrigger: UserStory, sourceEventId: EventId){
        if(executor.setResult(baseStory, storyToTrigger, executor.gotTriggered(storyToTrigger.getGivenEventName()), true)){
            storyToTrigger.registerOnExpectedResult(this.eventEngine);
            var nextEvents = storyToTrigger.getTriggeringEvents(triggeredBy);
            if(nextEvents == null){
                trace('Got null as next Events for ${baseStory.getName()} - Substep ${substepPosition + 1} - ${Classes.getFullName(Classes.fromInstance(triggeredBy))}!');
            } else {
                for(triggeringEvent in nextEvents){
                    this.eventEngine.triggerEvent(triggeringEvent, sourceEventId);
                }
            }
        } else {
            trace('Could not setResult for ${baseStory.getName()} - ${storyToTrigger.getName()} - ${Classes.getFullName(Classes.fromInstance(triggeredBy))}!');
        }
    }

    public function onSubstepWasProcessed(storyGotResult: UserStory, event: Event, pos: Int) {
        executor.setResult(this.baseStory, storyGotResult, executor.hasTriggeredTheSubstep(pos), true);
    }

    public function onSubstepErrorAccured(storyGotResult: UserStory, event: Event, pos: Int) {
        executor.setResult(this.baseStory, storyGotResult, executor.hasntExpectedSubResult(Type.getClass(event),pos), true);
    }

    public function onUserStoryGotExpectedEvent(storyGotResult: UserStory, event: Event){
        executor.setResult(this.baseStory, storyGotResult, executor.hasTriggered(event.__getEventName()), true);
        executor.setResult(this.baseStory, storyGotResult, executor.hasExpectedResult(), storyGotResult.isResultOk(event) );
    }
}

class HaxedaStoryStepsCalculator {
    public static function getStorySteps(
        allStories: List<UserStory>,
        targetStory: UserStory
    ) : List<UserStory> {
        var searchingStepsList = new Map<UserStory, Null<UserStory>>();
        var toCheckPuffer = new List<UserStory>();
        var ignoreStories = new List<UserStory>();
        var startingStory : UserStory = null;

        searchingStepsList.set(targetStory, null);
        toCheckPuffer.add(targetStory);
        ignoreStories.add(targetStory);

        while(!toCheckPuffer.isEmpty() && startingStory == null){
            var toCheck = toCheckPuffer.first();
            toCheckPuffer.remove(toCheck);

            for(possibleNewCheckStory in allStories){
                if(!ignoreStories.filter((it) -> { return it == possibleNewCheckStory; }).isEmpty()){
                    continue;
                }
                if(possibleNewCheckStory.getExpectedEventName() != toCheck.getGivenEventName()){
                    continue;
                }
                if(possibleNewCheckStory != targetStory && !possibleNewCheckStory.shouldTestBeSuccessful()){
                    continue;
                }
                searchingStepsList.set(possibleNewCheckStory, toCheck);
                toCheckPuffer.add(possibleNewCheckStory);
                ignoreStories.add(possibleNewCheckStory);
                break;
            }

            if(toCheck.getGivenEventName() == new ApplicationStarted().__getEventName()){
                startingStory = toCheck;
            }
        }
        if(startingStory == null) { return null; }

        var result = new List<UserStory>();

        while(startingStory != null) {
            result.add(startingStory);
            startingStory = searchingStepsList.get(startingStory);
        }

        return result;
    }
}

class HaxedaTestExecutor extends buddy.BuddySuite {
    private var storyCreators : Array<haxeda.test.UserStoryCreator>;
    private var stories : List<haxeda.test.UserStory>;
    private var testResults : Map<UserStory, Map<String,{step:Int,map:Map<String,Bool>}>>;
    private var eventEngineRegisterer : haxeda.EventEngineRegisterer;
    private var shouldBeSuccessful : Map<String, Bool>;
    private var engineSupervisors: Array<EventEngineSupervisor>;

    public function gotTriggered(event: String) : String{
        return 'Was Triggered by $event';
    }
    public function hasTriggered(event: String) : String{
        return 'Has Triggered the Event $event';
    }
    public function hasExpectedResult() : String{
        return 'Has the expected Result';
    }

    public function hasntExpectedSubResult(event: Class<Event>, pos: Int) : String {
        return 'Got an Error in Substep ${pos+1} when got ${Classes.getSimpleName(event)}';
    }

    public function hasTriggeredTheSubstep(pos: Int) : String {
        return 'Substep ${pos+1} got triggered';
    }

    private function initStoryResult(story: UserStory, value : Bool) : Map<String,Bool> {
        var storyResult = new Map<String,Bool>();

        storyResult.set(gotTriggered(story.getGivenEventName()), value);
        storyResult.set(hasTriggered(story.getExpectedEventName()), value);
        storyResult.set(hasExpectedResult(), value);
        for(substep in 0...story.getSubstepCount()){
            storyResult.set(hasTriggeredTheSubstep(substep), value);
        }

        return storyResult;
    }

    private function runUserstory(story: UserStory) {
        var storyResult = new Map<String,{step:Int,map:Map<String,Bool>}>();
        var stepResult = new Map<String,Bool>();
        var eventEngine = new EventEngine(null, this.engineSupervisors);
        var stepCounter = 1;

        var storySteps = HaxedaStoryStepsCalculator.getStorySteps(stories, story);

        stepResult.set("Calculating StorySteps was successfull", storySteps != null);


        var registeringResult = eventEngineRegisterer.registerToEventEngine(eventEngine);

        final initSuccessful = "Initializing EventEngine was successful";
        final prepSuccessful = "Preparation was successful";
        stepResult.set(initSuccessful, registeringResult);
        storyResult.set(prepSuccessful, {step: stepCounter, map: stepResult});
        shouldBeSuccessful.set(initSuccessful, true);
        shouldBeSuccessful.set(prepSuccessful, true);

        testResults.set(story, storyResult);

        if(registeringResult != true || storySteps == null){
            return;
        }

        new HaxedaTestListener(this, story, eventEngine, storySteps);

        for(step in storySteps){
            stepCounter++;
            stepResult = initStoryResult(step, false);
            storyResult.set(step.getName(), {step: stepCounter, map: stepResult});
        }

        var additionalListener : Null<EventListener> = null;

        if(story.get_timeToWaitForEndEvent() > 0){
            additionalListener = new UserStoryWaitListener(
                eventEngine,
                story.getGivenEventName(),
                story.getExpectedEventName(),
                story.get_timeToWaitForEndEvent()
            );
        }

        eventEngine.start(RunTilNoneWaiting);

        if(additionalListener != null){
            eventEngine.removeListener(additionalListener);
            additionalListener = null;
        }
    }

    public function new(
        eventEngineRegisterer: haxeda.EventEngineRegisterer,
        userStoryCreatorCollectionManager: haxeda.test.UserStoryCreatorCollectionManager,
        engineSupervisors: Array<EventEngineSupervisor> = null,
    ){
        this.storyCreators = userStoryCreatorCollectionManager.getAllCreators();
        this.eventEngineRegisterer = eventEngineRegisterer;
        this.engineSupervisors = engineSupervisors;

        stories = new List<haxeda.test.UserStory>();
        shouldBeSuccessful = new Map<String, Bool>();
        testResults = new Map<UserStory, Map<String,{step:Int,map:Map<String,Bool>}>>();


        //Execute Tests
        for(storyCreator in storyCreators){
            var story = storyCreator.getStory();
            stories.add(story);
            shouldBeSuccessful.set(story.getName(), story.shouldTestBeSuccessful());
        }

        //Execute Tests
        for(storyCreator in storyCreators){
            var testname = Classes.getSimpleName(Type.getClass(storyCreator));
            describe(testname,{
                var story = storyCreator.getStory();
                runUserstory(story);
                var result = testResults.get(story);
                var step = 0;
                var sortedKeys = [for(key in result.keys())key];
                sortedKeys.sort((a,b)->{
                    Reflect.compare(result[a].step, result[b].step);
                });
                for(storyStepName in sortedKeys){
                    step++;
                    it('$step: $storyStepName',{
                        var resultMap = result.get(storyStepName);
                        if(resultMap != null){
                            var wholeResult = true;
                            var shouldBeCorrect = true;
                            if(shouldBeSuccessful.get(storyStepName) == false){shouldBeCorrect = false;}
                            for(mapKey in resultMap.map.keys()){
                                var value = resultMap.map.get(mapKey);
                                if(value != true){
                                    wholeResult = false;
                                    if(shouldBeCorrect)fail(
                                        '"$mapKey" should be $shouldBeCorrect but was $value',
                                        story.getDefinitionPosition()
                                    );
                                }
                            }
                            if(wholeResult && !shouldBeCorrect){
                                fail(
                                    'None of the following steps failed: ${[for(key in resultMap.map.keys())key].join(", ")}',
                                    story.getDefinitionPosition()
                                );
                            }
                            true.should.be(true);
                        }
                        else{
                            fail("Result is null", story.getDefinitionPosition());
                        }
                    }, story.getDefinitionPosition());
                }
            });
        }
    }

    public inline function setResult(forStory: UserStory, storyStep: UserStory, stepName: String, result: Bool) : Bool {
        var fullMap = testResults.get(forStory);
        if(fullMap == null){
            shouldBeSuccessful.set(storyStep.getName(), true);
            testResults.set(forStory, [storyStep.getName() => {step: -1, map: ["Can Read Result-Map" => false]}]);
            return false;
        }
        var stepsMap = fullMap.get(storyStep.getName());
        if(stepsMap != null){
            if(stepsMap.map.get(stepName) == result){
                return false;
            }
            stepsMap.map.set(stepName, result);
            return true;
        } else {
            trace('stepsMap not Found for ${storyStep.getName()}');
            fullMap.set(storyStep.getName(), {step: -1, map: ['Can Read Result-Step-Map' => false]});
            return false;
        }
    }
}
