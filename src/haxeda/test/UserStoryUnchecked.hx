package haxeda.test;

import haxe.PosInfos;
import haxeda.events.Event;

enum WhenEventFun {
    DataEvent(fun: Event -> Null<Array<Event>>);
    UserEvent(fun: {} -> Array<Event>);
}

enum ThenEventFun {
    DataEvent(fun: Event -> Bool);
    UserEvent(fun: {} -> Bool);
}

class UserStoryUnchecked {
    private var storyName: String;
    private var givenEventName: String;
    private var whenEventFun: WhenEventFun;
    private var thenEventName: String;
    private var thenEventFun: ThenEventFun;
    private var inBetweenSteps : Array<{when: String, then: WhenEventFun}>;

    @:allow(UserStoryWhen)
    public function new(
        storyName: String,
        givenEventName: String,
        whenEventFun: WhenEventFun,
        thenEventName: String,
        thenEventFun: ThenEventFun,
        inBetweenSteps : Array<{when: String, then: WhenEventFun}>,
    ) {
        this.storyName = storyName;
        this.givenEventName = givenEventName;
        this.thenEventName = thenEventName;
        this.thenEventFun = thenEventFun;
        this.whenEventFun = whenEventFun;
        this.inBetweenSteps = inBetweenSteps;
    }

    private inline function toUserStory(withSuccess: Bool, ?pos : PosInfos) : UserStory {
        return new UserStory(
            storyName,
            givenEventName,
            whenEventFun,
            inBetweenSteps,
            thenEventName,
            thenEventFun,
            withSuccess,
            pos
        );
    }

    public function isSuccessful(?pos : PosInfos) : UserStory {
        return toUserStory(true, pos);
    }

    public function withExepctedError(?pos : PosInfos) : UserStory {
        return toUserStory(false, pos);
    }
}
