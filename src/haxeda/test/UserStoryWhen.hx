package haxeda.test;

import haxeda.test.UserStoryUnchecked.WhenEventFun;
import haxeda.events.Event;

@:generic
class UserStoryWhen {
    private var storyName: String;
    private var givenEventName: String;
    private var whenEventFunList: WhenEventFun;
    private var inBetweenSteps : Array<{when: String, then: WhenEventFun}>;

    @:allow(UserStoryGiven)
    public function new(storyName: String, givenEventName: String, whenEventFunList: WhenEventFun) {
        this.storyName = storyName;
        this.givenEventName = givenEventName;
        this.whenEventFunList = whenEventFunList;
        this.inBetweenSteps = [];
    }

    public function andWhen<When:Event>(when: Class<When>, then: When -> Null<Array<Event>>) {
        this.inBetweenSteps.push({when:Classes.getFullName(when),then:DataEvent(cast then)});
        return this;
    }

    public function andWhenUserEvent(when: String, then: {} -> Null<Array<Event>>) {
        this.inBetweenSteps.push({when:when,then:UserEvent(then)});
        return this;
    }


    @:generics
    public function then<Then:Event>(thenEvent: Class<Then>, thenTestFun: Then -> Bool): haxeda.test.UserStoryUnchecked {
        return new UserStoryUnchecked(
            storyName,
            givenEventName,
            whenEventFunList,
            Classes.getFullName(thenEvent),
            DataEvent(cast thenTestFun),
            inBetweenSteps
        );
    }

    public function thenUserEvent(eventName: String, thenTestFun:{} -> Bool): haxeda.test.UserStoryUnchecked {
        return new UserStoryUnchecked(
            storyName,
            givenEventName,
            whenEventFunList,
            eventName,
            UserEvent(thenTestFun),
            inBetweenSteps
        );
    }
}
