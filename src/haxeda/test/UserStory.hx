package haxeda.test;

import haxeda.test.UserStoryUnchecked;
import haxeda.events.Event;
import haxe.PosInfos;
import haxeda.EventListener;
import haxeda.events.order.EventId;

using extension.tools.OptionTools;

class UserStory implements EventListener{
    private var storyName : String;
    private var givenEventName : String;
    private var whenEventFun : WhenEventFun;
    private var inBetweenSteps : Array<{when: String, then: WhenEventFun}>;
    private var thenEventName : String;
    private var thenEventFun : ThenEventFun;
    private var shouldBeSuccessful : Bool;
    private var timeToWaitForEndEvent : Int;
    private var pos : PosInfos;

    private var eventEngine: EventEngine;

    public function getSubstepCount() : Int {
        return inBetweenSteps.length;
    }

    public function getExpectedSubstepEventName(pos: Int) : String {
        return inBetweenSteps[pos].when;
    }

    public function getSubstepEvents(pos: Int, whenEvent: Event) : Null<Array<Event>> {
        if(pos < 0 || pos >= inBetweenSteps.length)return null;
        var substep = inBetweenSteps[pos];
        switch(substep.then){
            case UserEvent(fun): return whenEvent.getData().map(data -> fun(data)).getOrDefault([]);
            case DataEvent(fun): return fun(whenEvent);
        }
    }

    public function get_timeToWaitForEndEvent() : Int {
        return this.timeToWaitForEndEvent;
    }

    public function getName(): String { return storyName; }

    public function getGivenEventName(): String {
        return this.givenEventName;
    }
    public function getExpectedEventName(): String {
        return this.thenEventName;
    }

    public function getTriggeringEvents(triggeredBy: Event): Array<Event> {
        switch(this.whenEventFun){
            case UserEvent(fun): return triggeredBy.getData().map(data -> fun(data)).getOrDefault([]);
            case DataEvent(fun): return fun(triggeredBy);
        }
    }
    public function isResultOk(eventToCheck: Event): Bool {
        switch(this.thenEventFun){
            case UserEvent(fun): return eventToCheck.getData().map(data -> fun(data)).getOrDefault(false);
            case DataEvent(fun): return fun(eventToCheck);
        }
    }
    public function shouldTestBeSuccessful() : Bool {
        return this.shouldBeSuccessful;
    }

    public function registerOnExpectedResult(eventEngine: EventEngine){
        this.eventEngine = eventEngine;
        eventEngine.registerGenericFunction(this.thenEventName, this, this.onEvent);
    }

    public function onEvent(eventOccured: Event, sourceEventId: EventId) : EventProcessingResult {
        if(eventEngine != null){
            eventEngine.removeGenericFunction(this.givenEventName, this);
            eventEngine = null;
        }
        return Success;
    }

    public function andWaitForEndEvent(maxWaitTimeInSec: Int) : UserStory {
        if(maxWaitTimeInSec > 0){
            this.timeToWaitForEndEvent = maxWaitTimeInSec;
        } else {
            this.timeToWaitForEndEvent = -1;
        }
        return this;
    }

    public function new (
        storyName: String,
        givenEventName: String,
        whenEventFun: WhenEventFun,
        inBetweenSteps : Array<{when: String, then: WhenEventFun}>,
        thenEventName: String,
        thenEventFun: ThenEventFun,
        shouldBeSuccessful: Bool,
        pos : PosInfos
    ) {
        this.storyName = storyName;
        this.givenEventName = givenEventName;
        this.thenEventName = thenEventName;
        this.thenEventFun = thenEventFun;
        this.whenEventFun = whenEventFun;
        this.inBetweenSteps = inBetweenSteps;
        this.shouldBeSuccessful = shouldBeSuccessful;
        this.timeToWaitForEndEvent = -1;
        this.pos = pos;
    }

    public function getDefinitionPosition() { return this.pos; }
}
