package haxeda.test;

interface UserStoryCreatorCollectionManager {
    public function getAllCreators() : Array<haxeda.test.UserStoryCreator>;
}
