package haxeda.test;

import haxeda.test.UserStoryGiven.UserStoryGivenUserEvent;
import haxeda.events.Event;

abstract class UserStoryCreator {
    private var story : haxeda.test.UserStory = null;
    private abstract function createStory() : haxeda.test.UserStory;
    public function getStory() : haxeda.test.UserStory {
        if(this.story == null){
            this.story = this.createStory();
        }
        return this.story;
    }

    @:generic
    private function given<Given:Event>(givenEvent: Class<Given>) : UserStoryGiven<Given> {
        return new UserStoryGiven<Given>(this, givenEvent);
    }
    private function givenUserEvent(givenEventName: String) : UserStoryGivenUserEvent {
        return new UserStoryGivenUserEvent(this, givenEventName);
    }
    private function givenAppStart() : UserStoryGivenUserEvent {
        return new UserStoryGivenUserEvent(this, "ApplicationStarted");
    }
}
