package haxeda.test;

import haxeda.EventListener.EventProcessingResult;
import haxeda.events.Event;
import haxeda.events.order.EventId;

class UserStoryWaitListener implements EventListener {
    private var endWaitTime : Float;
    private var gotThenEvent: Bool;
    private var maxWaitTime: Int;

    @:generic
    public function new<When:Event,Then:Event>(
        eventEngine: EventEngine,
        whenEventName: String,
        thenEventName: String,
        maxWaitTime: Int
    ) {
        this.maxWaitTime = maxWaitTime;
        gotThenEvent = false;
        endWaitTime = -1;
        eventEngine.registerGenericFunction(whenEventName, this, onWhenEvent);
        new UserStoryWaitListenerHelper(eventEngine, thenEventName, onThen);
    }

    private function onThen() {
        this.gotThenEvent = true;
    }

    private function onWhenEvent(_:Event, sourceEventId: EventId) : EventProcessingResult {
        if(endWaitTime < 0){
            gotThenEvent = false;
            endWaitTime = Date.now().getTime() + maxWaitTime * 1_000;
        }
        if(gotThenEvent || endWaitTime < Date.now().getTime()){
            return Success;
        } else {
            return StopProcessing(300);
        }
    }
}


class UserStoryWaitListenerHelper implements EventListener {
    private var callback: ()->Void;
    @:generic
    public function new<Then:Event>(
        eventEngine: EventEngine,
        thenEventName: String,
        callback: ()->Void,
    ) {
        this.callback = callback;
        eventEngine.registerGenericFunction(thenEventName, this, onThenEvent);
    }

    private function onThenEvent(_:Event, sourceEventId: EventId) : EventProcessingResult {
        callback();
        return Success;
    }
}
