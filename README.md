# HaxEDA-Test

Haxe-EDA implementation.

## Install

```
haxelib install haxeda-test
```

## Write your Tests

### 1. Create the TestMain

haxeda is using [buddy](https://github.com/ciscoheat/buddy) for testing. So first
create the main by implementing `buddy.Buddy` and instancing there the `HaxedaTestExecutor`:

```
package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    new haxeda.test.HaxedaTestExecutor(
        new MyEventEngineRegisterer(),
        new test.UserStoryCreatorManager()
    )
]>{}
```
The `HaxedaTestExecutor` requires 2 Parameters:
 1. `MyEventEngineRegisterer`, that is used to instance and bind all Listeners
 2. `UserStoryCreatorManager`, that is creating all Story-Instances

### 2. Create an `EventEngineRegisterer`

This Class should instance and register all Listeners and bind them to the `EventEngine`.

```
import haxeda.EventEngine;
import haxeda.EventEngineRegisterer;

class MyEventEngineRegisterer implements EventEngineRegisterer {
    public function new(){}

	public function registerToEventEngine(
        eventEngineToRegister:EventEngine
    ):Bool {
        var isRegisteringListenersSuccessful: Bool = false;
		#if test
            new TestOnlyListener1(eventEngineToRegister);
            new TestOnlyListener2(eventEngineToRegister);

			SingletonTestOnlyListener3.instance.setupEventEngine(eventEngineToRegister);
			SingletonTestOnlyListener4.instance.setupEventEngine(eventEngineToRegister);
		#else
            new ProductionOnlyListener1(eventEngineToRegister);
            new ProductionOnlyListener2(eventEngineToRegister);

			SingletonProductionOnlyListener3.instance.setupEventEngine(eventEngineToRegister);
			SingletonProductionOnlyListener4.instance.setupEventEngine(eventEngineToRegister);
        #end
        new Listener1(eventEngineToRegister);
        new Listener2(eventEngineToRegister);

        SingletonListener3.instance.setupEventEngine(eventEngineToRegister);
        SingletonListener4.instance.setupEventEngine(eventEngineToRegister);

        isRegisteringListenersSuccessful = true;

		return isRegisteringListenersSuccessful;
	}
}
```

This Class should be used also in the production code to ensure that all the same
Listeners are registered.

### 3 Create an `UserStoryCreatorManager`

This Class creates instances of all UserStories and returns a list of it:

```
package test;

class UserStoryCreatorManager implements haxeda.test.UserStoryCreatorCollectionManager{
    public function new() {}

    public function getAllCreators() : Array<haxeda.test.UserStoryCreator> {
        var param = getSomeParameter();
        return [
            new test.stories.UserStory1(),
            new test.stories.UserStory2(),
            new test.stories.UserStoryWithParameter1(param),
            new test.stories.UserStoryWithParameter2(param),
        ];
    }
}
```

### 4 Create the `UserStory`

This Class defines the actual Test. It has to be bind using the `UserStoryCreatorManager`.

```
package test.stories;
using haxeda.test.UserStory;

class ATestStartEventIsProcessedAnReturnsAnTestDoneEvent extends haxeda.test.UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return this.given(
                haxeda.events.ApplicationStarted
            ).when(
                (e) -> { return new test.events.TestStart(); }
            ).then(
                test.events.TestDone, (e) -> {
                    return true;
                }
            ).isSuccessful();
    }
}
```

The Test is divided in 4 Parts:
 1. The `given` part: This part defines, what event is expected, to start the test.
    The predefined Event `ApplicationStarted` is called after the `EventEngine` was set up
    and started.
 2. The `when` part: This gets the expected event (Type defined in `given`-part) and
    creates a new Event, that will be process by some bound listener.
 3. The `then` part: This part is listening for a specific type of event and tests, if
    the event and the processing is correct. If this event wasn't catched, this part won't
    be executed and the `then` part will be handelt as `false`.
 4. `isSuccessful`/`withExepctedError` defines, if the `then` part should be successfull
    or not

### 5 Chaining multiple tests together

If in the `given` part is defined an Event that is not `ApplicationStarted`, then the
`HaxedaTestExecutor` will chain tests together, to get the expected `Event`.

Example:
```
class EverythingGetsInitialized extends UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return  this.given( ApplicationStarted )
                    .when( (e) -> { return new InitializeEverything(); }  )
                    .then( InitializeDone, (e) -> {
                        return true;
                    })
                    .isSuccessful();
    }
}
class ShowTheMainMenuAfterInit extends UserStoryCreator {
    public function new(){}
    public function createStory() : UserStory {
        return  this.given( InitializeDone )
                    .when( (e) -> { return new ShowMainMenu(); }  )
                    .then( MainMenuIsShown, (e) -> {
                        return true;
                    })
                    .isSuccessful();
    }
}
```

When using this 2 Testcases (there has to be some listeners, that are listening
to the events `InitializeEverything` and `ShowMainMenu`, and sending after processing the
corespondening events `InitializeDone` or `MainMenuIsShown`), there will be executed 2
UserStories:
 1. `EverythingGetsInitialized`: that executes only itself
 2. `ShowTheMainMenuAfterInit`: that first executes `EverythingGetsInitialized` to
    create a `InitializeDone` event, and then the testcase itself

### 6 How to understand the Tests

The tests are written in a way, that it can replace the User Interface and emit the identical
events.

You can write a testcase to replace the following scenario:

On arrival of the Event `ShowSettingsWindow`, the UI normaly showes a new Window with a button
`Save`. If the user clicks on the Button, it will create an Event `SaveSettings` with all the
data inside the Textfields.
After the saving action is done, the UI receives a `SaveSettingsDone` event, ether with a
saving ok option or an error message.

To recreate in a testcase first make sure, there is no UI in the testcases!

Than you can create a Testcase, the has the `ShowSettingsWindow` in the `given` part and creates a `SaveSettings` in the `when` part. In the `then` part you can check the
`SaveSettingsDone` event, if it was successfull or got an error and checking, if the
expected case happened.

### Important things to notice

If you are creating a good and bad case, **always first instance the good case** in the `UserStoryCreatorManager`. If there are mutliple Tests, that expects an exent in the `then` part, **always the fist Story that provides this event will be used for chaining tests**!

## Write your business logic

Use [`haxeda`](https://gitlab.com/dani_987/haxeda) to write your business logic. You can find there more information about it as well.
